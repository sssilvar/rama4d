from setuptools import setup


def readme():
    with open('README.md') as f:
        return f.read()


def requirements():
    with open('requirements.txt') as rq:
        return rq.read().split('\n')


setup(
    name='rama4d',
    version='1.0.0',
    description='Librería para codificar automáticamente texto en CIIU Rev. 4 A.C.',
    long_description=readme(),
    long_description_content_type='text/markdown',
    classifiers=[
        'Development status :: 5 - Production/Stable',
        'License :: OSI Approved  :: MIT License',
        'Programming Language :: Python :: 3.6',
        'Operanting System :: OS Independent'
    ],
    url='https://gitlab.com/sssilvar/rama4d',
    author='Santiago Smith Silva R.',
    author_email='sssilvar@dane.gov.co',
    keywords='nlp dane ciiu4 clasificación automática',
    license='MIT',
    packages=['rama4d'],
    package_dir={'rama4d': 'rama4d'},
    package_data={'rama4d': ['models/lemma_with_stop_corrected/*']},
    install_requires=requirements(),
    include_package_data=True,
    python_requires='>=3.6'
)
