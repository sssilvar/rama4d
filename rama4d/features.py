import re
import unicodedata

import numpy as np
import pandas as pd
import spacy

# Definir un procesador de texto
from .models import load_spacy_model

SPACY_MODEL = 'es_core_news_md'
nlp = load_spacy_model(SPACY_MODEL)


def get_nlp():
    """
    Retorna un objeto procesador de texto (NLP).
    :return: modelo procesador de SpaCy
    """
    return nlp


def num2text(value, var):
    value = int(value)
    try:
        if var == 'P6880':
            fields = {
                1: 'en_vivienda_propia',
                2: 'en_otras_viviendas',
                3: 'En_kiosco_o_caseta',
                4: 'En_un_vehículo',
                5: 'Puerta_a_puerta',
                6: 'Ambulante',
                7: 'En_local_u_oficina',
                8: 'En_el_campo_área_rural_mar_o_río',
                9: 'En_una_obra_de_construcción',
                10: 'En_una_mina_o_cantera',
                11: ''
            }
            return strip_accents(fields[value].lower())
        elif var == 'P6430':
            fields = {
                1: 'Obrero_de_empresa_particular',
                2: 'Servidor_público',
                3: 'Empleado_doméstico',
                4: 'Independiente',
                5: 'Patrón_o_empleador',
                6: 'Trabajador_familiar_sin_remuneración',
                7: 'Trabajador_familiar_sin_remuneración_de_empresas_o_negocios_de_otros_hogares',
                8: 'Jornalero_o_peón',
                9: ''
            }
            return strip_accents(fields[value].replace(" ", "_").lower())
    except KeyError:
        return f''


def clean_dataframe(df):
    """
    Cambia nombres de columnas a mayúsculas y elimina datos perdidos.

    :param pd.DataFrame df: Dataframe a procesar.
    :return pd.DataFrame: Dataframe preprocesado y limpio
    """
    # Actualizar nombres de columnas a mayúsculas
    df.columns = [str(col).upper() for col in df.columns]

    # Limpiar NaNs (datos perdidos)
    df = df.dropna(axis='rows', how='all')
    df.fillna(0, inplace=True)
    return df


def extract_text(df):
    # Generar texto a partir de campos de posición ocupacional (P6430)
    # Y de lugar donde realiza la actividad (P6880)
    series_p6430 = df['P6430'].fillna(0).astype(int).apply(num2text, var='P6430')
    series_p6880 = df['P6880'].fillna(0).astype(int).apply(num2text, var='P6880')

    # Concatenar Profesión y Actividad
    text_joined = df['P6370'].astype(str) + '. ' + \
                  df['P6390'].astype(str) + '. ' + \
                  series_p6430 + '. ' + \
                  series_p6880
    return text_joined


def lowercase(text):
    """
    Returns a text in lowercase.

    :param str text: Text to be processed
    :return str: Lowercase text
    """
    return text.lower()


def remove_numbers(text):
    """
    Removes numbers from text.

    :param text: Input text.
    :return: Text with numbers removed.
    """
    return re.sub(" \d+", " ", text)


def remove_spaces(text):
    """
    Removes multiple spaces and keeps a single-space.

    :param text: Input text
    :return: Text with removed spaces
    """
    text = re.sub(" +", " ", text)
    if text.startswith(' '):
        text = text[1:]
    if text.endswith(' '):
        text = text[:-1]
    return text


def strip_accents(text):
    """
    Elimina acentos en las palabras (tíldes y demás).

    :param str text: Texto a eliminarle los acentos.
    :return: Texto sin acentos
    """
    return ''.join(c for c in unicodedata.normalize('NFD', text)
                   if unicodedata.category(c) != 'Mn')


def remove_punctuation(text):
    """
    Elimina cualquier signo de puntuación presente en el texto.
    A exepción de puntos (separan oraciones) y rayas al piso (une tokens).

    :param str text: Texto a removerle signos.
    :return: Texto procesado
    """
    return re.sub(r'[^ a-zA-Z0-9._]', ' ', text)


def lemmatize(text):
    """
    Lematizar texto.

    :param str text: Texto a lematizar.
    :return: Texto procesado.
    """
    doc = nlp(text)
    lemmas = [ent.lemma_ for ent in doc]
    return ' '.join(lemmas)


def preprocess_text(text):
    """
    Preprocesar un texto usando un pipeline definido.
    Incluye: pasar a minúsculas, eliminar números, eliminar acentos,
    eliminar puntuación y eliminar espacios repetidos.

    :param str text: Texto a preprocesar.
    :return str: Texto preprocesado.
    """
    doc = nlp(text)
    tokens = [remove_punctuation(strip_accents(token.lemma_.lower()))
              for token in doc
              if not token.is_left_punct
              and not token.is_right_punct
              and not token.is_space]
    return remove_spaces(' '.join(tokens))
