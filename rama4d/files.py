from tkinter import Tk, filedialog


def select_file():
    # Crear una ventana de sistema operativo
    root = Tk()
    root.withdraw()
    # Argumentos para que la ventana sea flotante al frente
    root.call('wm', 'attributes', '.', '-topmost', True)
    # Retornar el archivo seleccionado
    return filedialog.askopenfilename(multiple=False, filetypes=[("Archivos de Excel", "*.xlsx *.xls"), ("Archivos CSV", "*.csv")])
