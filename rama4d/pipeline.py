import pickle
import pandas as pd
import os
from tqdm import tqdm
from os.path import join, abspath, dirname, basename, realpath, isdir

from keras.models import model_from_json, load_model
from keras.preprocessing.sequence import pad_sequences

from .features import preprocess_text

#####################################
# CONFIGURACION DEL ENTORNO
#####################################

tqdm.pandas()  # Barras de progreso

# Carpetas y archivos concernientes al modelo
ROOT = dirname(realpath(__file__))
try:
    MODEL_FOLDER = os.environ['MODEL_FOLDER']
    print('[  INFO  ] Usando modelo externo:', MODEL_FOLDER)
    assert isdir(MODEL_FOLDER), f'No es una carpeta válida. Por favor verificar:\n\t- {MODEL_FOLDER}'
except KeyError:
    MODEL_FOLDER = join(ROOT, 'models', 'lemma_with_stop_corrected')
    print('[  INFO  ] Usando modelo interno:', MODEL_FOLDER)

MODEL_FILE = join(MODEL_FOLDER, 'lstm_weights.json')
WEIGHTS_FILE = join(MODEL_FOLDER, 'lstm_weights.h5')
TOKENIZER_FILE = join(MODEL_FOLDER, 'tokenizer.sav')
FEATURE_SIZE_FILE = join(MODEL_FOLDER, 'features_size.sav')
LABELS_FILE = join(MODEL_FOLDER, 'labels.sav')

# Load Objects
print('Loading model...')
tokenizer = pickle.load(open(join(TOKENIZER_FILE), 'rb'))
labels = pickle.load(open(join(LABELS_FILE), 'rb'))
feature_size = pickle.load(open(join(FEATURE_SIZE_FILE), 'rb'))
with open(MODEL_FILE, 'r') as jm:
    json_model = jm.read()

model = model_from_json(json_model)
model.load_weights(WEIGHTS_FILE)
print(model.summary())


def predict(text):
    text = preprocess_text(text)
    x_test = tokenizer.texts_to_sequences([text])
    x_test = pad_sequences(x_test, maxlen=feature_size)

    y_prob = model.predict(x_test)
    ix_label = y_prob.argmax(axis=-1)

    y_pred = labels[ix_label][0]
    y_pred_proba = y_prob[:, ix_label].ravel()[0]

    return pd.Series({'RAMA4D_R4_PRED': y_pred, 'PROBA': y_pred_proba})


def predict_text_series(text_series):
    predictions = text_series.progress_apply(predict)
    predictions['RAMA4D_R4_PRED'] = predictions['RAMA4D_R4_PRED'].astype('int')  # Pasar coódigo CIIU a entero
    return predictions
