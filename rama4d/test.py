# # Test SpaCy
# from models import load_spacy_model
# print('Testing SpaCy...')
# nlp = load_spacy_model('es_core_news_sm')
# doc = nlp('Éste es un texto de ejemplo.')

# print([token.text for token in doc])
# print([token.lemma_ for token in doc])

# Test Keras model
# from pipeline import predict
# print('\n\nTesting Keras model')
# text = 'Vendedor de abrratotes. Vende abarrotes al por menor. En_local_u_oficina. Independiente'
# print(f'\t- Prueba: {text}')
# print(predict(text))

# Test Training model
print('Reading databases...')
from lstm import train_model
training_file = '/home/ssilvari/Downloads/BASE LIMPIA SANTIAGO - Consolidado_Revision_ocup_1601_1608_20171030_F - 20NOV19.xlsx'
train_model(training_file)