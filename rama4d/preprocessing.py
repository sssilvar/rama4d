import warnings
from os.path import splitext

from tqdm import tqdm
import pandas as pd

from .features import num2text
from .features import preprocess_text as prep_text, clean_dataframe

warnings.filterwarnings("ignore")
tqdm.pandas()


def read_database(filename, sep=','):
    ext = splitext(filename)[-1].lower()
    if ext.endswith('.xlsx') or ext.endswith('.xls'):
        return pd.read_excel(filename)
    elif ext.endswith('.csv'):
        return pd.read_csv(filename, low_memory=False, sep=sep)
    else:
        raise IOError('Archivo inválido. Debe ser un .xls, .xlsx, o .csv')


def extract_text(df):
    # Generar texto a partir de campos de posición ocupacional (P6430)
    # Y de lugar donde realiza la actividad (P6880)
    series_p6430 = df['P6430'].fillna(0).astype(int).apply(num2text, var='P6430')
    series_p6880 = df['P6880'].fillna(0).astype(int).apply(num2text, var='P6880')

    # Concatenar Profesión y Actividad
    text_joined = df['P6370'].astype(str) + '. ' + \
                  df['P6390'].astype(str) + '. ' + \
                  series_p6430 + '. ' + \
                  series_p6880
    return text_joined


def preprocess_text(text):
    return prep_text(text)


def preprocess_dataframe(df):
    # Limpiar DataFrame (imputar NaNs)
    df = clean_dataframe(df)

    # Extraer variables de interes en una cadena simple de carácteres
    text_series = extract_text(df)

    # Preprocesar cada campo
    return text_series.progress_apply(prep_text)


def join_predictions_to_dataframe(predictions, df):
    return pd.concat([df, predictions], axis='columns')


def save_processed_dataframe(df, filename):
    new_filename = '_pred'.join(splitext(filename))
    ext = splitext(filename)[-1].lower()
    print('Archivo codificado será guardado en:\n\t-', new_filename)

    if ext.endswith('.xls') or ext.endswith('.xlsx'):
        df.to_excel(new_filename)
    elif ext.endswith('csv'):
        df.to_csv(new_filename)
    return new_filename
