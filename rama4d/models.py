import os
import sys
import spacy

VENV = hasattr(sys, 'real_prefix')  # Flag for Virtual environment


def load_spacy_model(model='es_core_news_md'):
    params = '--no-deps' if VENV else '--user'
    print(f'Descargando modelo de Spacy...')
    spacy.cli.download(model, False, (params))
    return spacy.load(model)
