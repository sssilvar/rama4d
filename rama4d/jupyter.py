import ipython_blocking
from ipywidgets.widgets import FileUpload, Button, HBox, Layout, HTML


def upload_file_widget():
    """Retorna un Widget para subir archivos"""
    btn_lyt = Layout(width='25%', height='3em')

    upload = FileUpload(accept='.xls, .xlsx, .csv', description='Subir Base de datos', layout=btn_lyt)
    button = Button(description='Iniciar procesamiento!', icon='play', button_style='success', layout=btn_lyt)

    return HBox([upload, button])


def upload_file(widget):
    """Sube archivo a entorno virtual"""
    upload = widget.children[0]  # Botón de subida
    assert len(upload.value.keys()) == 1, 'Por favor reiniciar proceso y subir un archivo'
    filename = list(upload.value.keys())[0]

    # Escribir a disco en entorno virtual
    with open(filename, 'wb') as f:
        f.write(upload.data[0])
    return filename


def download_result(out_file):
    btn = f'''
    <form action="{out_file}" method="get" target="_blank">
        <button type="submit">Descargar base de datos procesada</button>
    </form>'''
    return HTML(value=btn)
