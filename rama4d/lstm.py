import pandas as pd
from tqdm import tqdm
from datetime import date

import os
import pickle
from os.path import join, basename, splitext, dirname

from keras.layers import Dense, Embedding, LSTM
from keras.models import Sequential
from keras.preprocessing.sequence import pad_sequences
from keras.preprocessing.text import Tokenizer

from .preprocessing import extract_text, read_database, preprocess_dataframe

# Environment setup
tqdm.pandas()
today = date.today()


def save_to_disk(filename, obj):
    with open(filename, 'wb') as handle:
        pickle.dump(obj, handle, protocol=pickle.HIGHEST_PROTOCOL)


def train_model(database_file, epochs=5):
    # Leer base de datos en formato Excel o CSV
    df = read_database(database_file)
    print(df.head())

    # Crear carpeta de salida (donde se guardan los pesos y los modelos)
    out_folder = join(dirname(database_file), 'weights_' + str(today))
    os.makedirs(out_folder, exist_ok=True)

    # Preprocesar base de datos
    text_series = preprocess_dataframe(df)
    print(text_series.head())

    # Tokenizar
    print('Tokenizing...')
    n_words = 4000
    tokenizer = Tokenizer(num_words=n_words, split=' ')

    tokenizer.fit_on_texts(text_series)
    X = tokenizer.texts_to_sequences(text_series)
    X = pad_sequences(X)
    print(f'Features dimensions: {X.shape}')

    # Guardar Tokenizer
    tokenizer_file = join(out_folder, 'tokenizer.sav')
    save_to_disk(tokenizer_file, tokenizer)

    features_size_file = join(out_folder, 'features_size.sav')
    save_to_disk(features_size_file, X.shape[1])

    # Etiquetas
    # CIIU Rev. 4
    print('Labels\n', df['RAMA4D_R4'].head())
    one_hot_labels = pd.get_dummies(df['RAMA4D_R4'])
    labels = one_hot_labels.columns
    Y = one_hot_labels.values

    # Guardar etiquetas
    labels_file = join(out_folder, 'labels.sav')
    save_to_disk(labels_file, labels)

    # =============================================
    # MODEL (LTSTM)
    # =============================================
    # Params
    embed_dim = 128
    lstm_out = 300
    batch_size = 64

    # Model
    model = Sequential()
    model.add(Embedding(n_words, embed_dim, input_length=X.shape[1], dropout=0.1))
    model.add(LSTM(lstm_out, dropout_U=0.1, dropout_W=0.1))
    model.add(Dense(labels.shape[0], activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

    # Entrenar modelo
    print(f'Entrenando modelo...')
    model.fit(X, Y, batch_size=batch_size, epochs=epochs)

    # Archivos a guardar del modelo
    weights_base = join(out_folder, 'lstm_weights')
    model_json_file = weights_base + '.json'
    weights_hdf = weights_base + '.h5'

    # Serializar arquitectura del modelo en JSON
    model_json = model.to_json()
    with open(model_json_file, "w") as json_file:
        json_file.write(model_json)

    # Guadrar pesos del modelo en HDF5
    model.save_weights(weights_hdf)
