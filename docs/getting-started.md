# Instalación de entorno para rama4d

Ésta guía instalación se hace una única vez por máquina.

* [Guía para Anaconda/Miniconda (Recomendado)](#anacondaminiconda)

* [Guía para Pure Python en Windows](#pure-python-en-windows)

* [Guía para Pure Python en Linux](#pure-python-en-linux)

* [Descarga de Software - Notebooks](#descargar-notebooks)

* [Recomendaciones](#recomendaciones)

* [Preguntas frecuentes (FAQ)](#faq)

## Anaconda/Miniconda

Anaconda es un software creado por la empresa continuum que empaca Python junto con otros paquetes adicionales. Miniconda es una versión liviana de Anaconda. Adicionalmente, Anaconda también es multi-plataforma y hace un mejor manejo de dependencias que pure Python.

### Creación y activación de entorno

Despues de instalar Anaconda/Miniconda, abrir **Anaconda Prompt** y crear un entorno virtual ejecutando:

```bash
conda create --name ciiu_env
```

Activar el entorno usando:

```bash
conda activate ciiu_env
```

### Instalar dependencias en entorno

Luego, instalar dependencias manualmente:

```bash
conda install -y spacy pandas keras git jupyter matplotlib
```

Descargar los modelos de Deep Learning de SpaCy para NLP:

```bash
python -m spacy download es_core_news_md
```

Y finalmente, instalar el package de `rama4d`:

```bash
pip install -U git+https://gitlab.com/sssilvar/rama4d.git plyer
```

Para ejecutar los notebooks ejecute:

```bash
jupyter notebook
```

¡Y listo! Puede proceder a [descargar los notebook y correr el software](#descargar-notebooks).

## Pure Python en Windows

Se recomienda usar python 3.6.8 para windows ([Descargar aquí](https://www.python.org/ftp/python/3.6.8/python-3.6.8-amd64.exe)). 

Es necesario agregar la carpeta que contiene los ejecutables como variable de entorno `PATH`:

```shell
setx path "%path%;%HOMEPATH%\AppData\Local\Programs\Python\Python36;%HOMEPATH%\AppData\Roaming\Python\Python36\Scripts;%HOMEPATH%\AppData\Local\Programs\Python\Python36\Scripts"
```

**Nota:** Se recomienda reiniciar sesión (o el PC de ser posible). Esto permite que se actualice el directorio de variables de entorno.

### Actualización de gestor de paquetes

Nos aseguramos de tener la última versión del gestor de paquetes de python **Pip**. Para esto, ejecutamos:

```shell
python -m pip install --upgrade --force-reinstall --no-cache pip
```

Procedemos a instalar las dependencias con los siguientes comandos:

```shell
python -m pip install -U --no-cache --force-reinstall jupyter plyer
python -m pip install -U --no-cache --force-reinstall https://gitlab.com/sssilvar/rama4d/raw/master/wheels/rama4d-1.0.0-py3-none-any.whl
```

¡Y listo! Puede proceder a [descargar los notebook y correr el software](#descargar-notebooks).

## Pure Python en Linux

Todas las distribuciones linux suelen venir con alguna versión de Python. Verifique la existencia y versión de python en su distribución utilizando:

```bash
python3 --version  # Versión de Python
pip3 --version  # Versión del gestor de paquetes
```

Si no cuenta con una instalación de Python 3 puede hacerlo ejecutando como administrador:

```bash
sudo apt-get install python3 python3-dev python3-tk  # En Distribuciones Debian
sudo dnf install python3 python3-devel python3-tkinter  # En distribuciones RedHat
```

Verifique que cuenta con una versión de Python >= 3.6 y de pip >=19.0. Actualice el gestor de paquetes ejecutando:

```bash
pip3 install --upgrade --user pip
```

Realice la instalación de dependencias:

```bash
pip install jupyter plyer
pip install -U https://gitlab.com/sssilvar/rama4d/raw/master/wheels/rama4d-1.0.0-py3-none-any.whl
```

¡Y listo! Puede proceder a [descargar los notebook y correr el software](#descargar-notebooks).

## Descargar Notebooks

Finalmente, procedemos a descargar y descomprimir los notebooks en el siguiente link: [**Descargar notebooks**](https://gitlab.com/sssilvar/rama4d/-/archive/master/rama4d-master.zip?path=demos).

Los siguientes archivos se encontrarán en la carpeta `demos`:

- `predecir-dataset.ipynb`: Notebook para codificar bases de datos CSV/Excel.

- `predecir-formulario.ipynb`: Un formulario interactivo para codificar con las variables de interés.

- `entrenar-modelo.ipynb`: Notebook para entrenar el modelo con una nueva base de datos.

- `experimentar-modelo`: Notebook para realizar experimentación (arquitectura, preprocesamiento y demás).

Una vez descargado y descomprimido, es necesario ejecutar en consola:

```bash
jupyter notebook
```

Esto lanzará una pestaña en su navegador por defecto con un explorador de archivos. Abra el notebook de interés desde allí.

Para hacer uso del software diríjase a: [**Guía del usuario - Codificación CIIU4**](./manual-de-uso.md)

## Recomendaciones

No se recomienda alterar los tres (3) primeros notebooks. Puede llevar a la generación de errores de sistema y/o codificación de no hacerce correctamente.

Todo tipo de experimentación, se recomienda hacerse en el notebook `experimentar-modelo`.

# FAQ

## ¿Cómo puedo eliminar un entorno?

Puede eliminarlo usando:

```bash
conda env remove -n NOMBRE_DE_ENTORNO
```

## Failed to create Anaconda menus or Failed to add Anaconda to the system PATH. ¿Qué hago?

Lo más probable es que su sistema no tiene Visual C++. Puede descargarlo aquí: [https://www.microsoft.com/en-pk/download/details.aspx?id=48145](https://www.microsoft.com/en-pk/download/details.aspx?id=48145)

### ¿Cómo puedo salir del entorno?

Si desea ejecutar otro software y necesita salir del entorno puede utilizar:

```bash
conda deactivate
```
