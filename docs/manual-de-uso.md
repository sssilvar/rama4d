# Manual de uso - Clasifiación automática CIIU4

Ésta sección contiene el manual de uso para la herramienta de clasifiación automática en CIIU Rev. 4 A.C.

## Preparación de entorno

Es necesario preparar el entorno para poder usar cualquiera de los Notebooks. para ello, abra **Anaconda Prompt** y active el entorno usando:

```bash
conda activate ciiu_env
```

Luego, inicie Jupyter Notebooks para ejecutar el software.

```bash
jupyter notebook
```

Abra el notebook de interés y siga las instrucciones.

## Clasificación automática de una base de datos Excel/CSV

Para esto, haremos uso del notebook `predecir-dataset.ipynb`. Éste notebook contiene un pipeline de clasificación automática en CIIU4 para bases de datos en excel o CSV. Es necesario que:

* Debe ser un archivo `.xls`, `.xlsx` o `.csv`

* La primera columna debe contener el nombre de las variables

* Debe contener como mínimo las siguientes variables:
  
  * **P6370**: ¿Qué hace en éste trabajo?
  
  * **P6390**: ¿A qué actividad se dedica principalmente la empresa/negocio donde trabaja?
  
  * **P6430**: En éste trabajo es... (posición ocupacional)
  
  * **P6880**:  ¿En qué lugar realiza la actividad?

### Instrucciones de uso

Después de abrir el notebook, y de tener la base de datos lista, presione el botón con el ícono de doble flecha:![](img/run-all-icon.png) ubicado en la parte superior izquierda.

Se abrirá una ventana para seleccionar el archivo a procesar. 

![](img/picker.png)

Selecciónelo y espere a que el notebook termine de procesar. Puede dejar el proceso corriendo y una notificación aparecerá en la barra de tareas apenas termine de procesar.

![](img/notification.png)


