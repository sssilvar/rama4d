# Instalación de entorno para rama4d

## Rama4d: Instalación en Anaconda y Miniconda

## Creación y activación de entorno

Despues de instalar Anaconda/Miniconda, abrir **Anaconda Prompt** y crear un entorno virtual ejecutando:

```bash
conda create --name ciiu_env
```

Activar el entorno usando:

```bash
conda activate ciiu_env
```

## Instalar dependencias en entorno

Luego, instalar dependencias manualmente:

```bash
conda install -y spacy pandas keras git jupyter
```

Descargar los modelos de Deep Learning de SpaCy para NLP:

```bash
python -m spacy download es_core_news_md
```

Y finalmente, instalar el package de `rama4d`:

```bash
pip install git+https://gitlab.com/sssilvar/rama4d.git
pip install plyer
```

Para ejecutar los notebooks ejecute:

```bash
jupyter notebook
```

## Salir de entorno (de ser necesario)

Si desea ejecutar otro software y necesita salir del entorno puede utilizar:

```bash
conda deactivate
```

## FAQ

#### ¿Cómo puedo eliminar un entorno?

Puede eliminarlo usando: 

```bash
conda env remove -n NOMBRE_DE_ENTORNO
```

## Failed to create Anaconda menus or Failed to add Anaconda to the system PATH. ¿Qué hago?

Lo más probable es que tu sistema no tiene Visual C++. Puede descargarlo aquí: [https://www.microsoft.com/en-pk/download/details.aspx?id=48145](https://www.microsoft.com/en-pk/download/details.aspx?id=48145)
