# Configurando contenedor Docker

Despues de instalar docker, agregamos el usuario actual al grupo de administradores Docker:

```bash
sudo usermod -aG docker ssilvari
```

Luego, reiniciamos e iniciamos el servicio de Docker:

```bash
sudo service docker start
```

Finalmente activamos el daemon de Docker para que inicie cada vez que la máquina inicie.

```bash
sudo systemctl enable docker
```

Iniciar contenedor con JupyterLab usando:

```bash
docker run --name ciiu -d -p 10000:8888 --memory=4g --cpuset-cpus=1 --restart='always' -e JUPYTER_ENABLE_LAB=yes -e RESTARTABLE=yes jupyter/base-notebook
```

Para visualizar el token de sesión y asignar una contraseña:

```bash
docker logs ciiu

```
