#!/usr/bin/env bash
echo "[ INFO ] Removing old wheels..."
eval "rm -rf dist/"

echo "[ INFO ] Packing new wheels..."
eval "python3 setup.py sdist bdist_wheel"

echo "[ INFO ] Moving new wheels to the 'wheels' directory..."
eval "cp dist/* wheels/"

echo "DONE!"